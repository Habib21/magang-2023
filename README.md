# Magang 2023

## Name
Nama    : Habib Aditiya Julianto                                     
NIM     : 2000016047                                                       
Posisi  : System Analyst                                                
Lokasi  : PT. Woolu Aksara Maya                

## Description
Melakukan analisis proses bisnis, dokumentasi, Research and Development.

## Support
Pembimbing Lapangan : Irfan Adam, S.T

## Roadmap
- [X] [1 | Analisis Data Requirement](https://gitlab.com/Habib21/magang-2023/-/milestones/2#tab-issues)
- [X] [2 | Analisis Proses Bisnis](https://gitlab.com/Habib21/magang-2023/-/milestones/3#tab-issues)
- [X] [3 | Rule dan regulasi](https://gitlab.com/Habib21/magang-2023/-/milestones/4#tab-issues)
- [X] [4 | Desain Produk dan Dashboard S-LIME](https://gitlab.com/Habib21/magang-2023/-/milestones/5#tab-issues)
- [X] [5 | Sistem Manajemen Bisnis SIPLAH](https://gitlab.com/Habib21/magang-2023/-/milestones/6#tab-issues)
- [X] [6 | Rancangan Arsitektur IT](https://gitlab.com/Habib21/magang-2023/-/milestones/7#tab-issues)

## Project status
On Progress
